from  PIL import Image
import fnmatch
import os.path
import shutil
import os

def compute_average_image_color(img):
    width, height = img.size
 
    r_total = 0
    g_total = 0
    b_total = 0

    count = 0
    for x in range(0, width):
        for y in range(0, height):
            r, g, b = img.getpixel((x,y))
            r_total += r
            g_total += g
            b_total += b
            count += 1

    return (r_total/count, g_total/count, b_total/count)

new_patch = f"{os.getcwd()}\\red_images"
if not os.path.exists(new_patch):
    os.makedirs(new_patch)

for one_file in os.scandir("Source"):
    if one_file.is_file():
        print(one_file.name)
        img = Image.open(one_file.path)
        average_color = compute_average_image_color(img)
        print(average_color)
        if average_color[0] > average_color[1] and average_color[0] > average_color[2]:
            shutil.copyfile(one_file.path, f"{new_patch}\\{one_file.name}")